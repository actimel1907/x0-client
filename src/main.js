import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
//import VueSocketIO from "vue-socket.io";
import Cell from "./components/Cell.vue";
import Board from "./components/Board.vue";

Vue.config.productionTip = false;

Vue.component("cell", Cell);
Vue.component("board", Board);
/*
Vue.use(
  new VueSocketIO({
    debug: true,
    connection: "https://localhost:8080"
  })
);*/

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
